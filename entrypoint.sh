#!/bin/sh

# cron
python manage.py makemigrations
python manage.py migrate
# gunicorn -c gunicorn_config.py 

exec "$@"
