# Generated by Django 5.0.6 on 2024-06-27 02:44

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='target',
            name='alias',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AlterField(
            model_name='target',
            name='custom_selector',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='target',
            name='custom_selector_type',
            field=models.CharField(blank=True, choices=[('css selector', 'Css'), ('xpath', 'Xpath'), ('tag name', 'Tag'), ('class name', 'Class')], max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='target',
            name='selector',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='core.defaultselector'),
        ),
        migrations.AlterField(
            model_name='target',
            name='status',
            field=models.CharField(blank=True, choices=[('S', 'Success'), ('O', 'Out of stock'), ('N', 'No selector set'), ('P', 'Price not found'), ('C', 'Cached'), ('U', 'Undefined status')], default='S', max_length=1, null=True),
        ),
    ]
