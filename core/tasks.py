import os
from prihud.celery import app
from core.models import Target
from core.scraping.impl.scraping_job import ScrapingJob


@app.task(queue="scraping")
def scrape(frequency: str = None, ids: list[int] = None) -> str:
    if ids:
        targets = Target.objects.filter(pk__in=ids).all()
    else:
        targets = Target.objects.filter(frequency=frequency).all()

    if len(targets) == 0:
        return "Found no targets for this scraping job"

    report = ScrapingJob(targets).start()
    os.system("killall -9 geckodriver")
    os.system("killall -9 firefox-esr")

    result = {}

    result["report"] = report

    if len(report["failures"]) > 0:
        result["errors"] = f'''
            === Had {len(report['failures'])} failures ===
            {[f"{failure[0].alias or failure[0].url}: {failure[1]}" for failure in failures]}
        '''

    return result