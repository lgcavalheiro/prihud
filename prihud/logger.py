import apprise
import logging
from prihud.settings import DISCORD_HOOK


class AppriseLogger:
    __apr = None
    __logger = logging.getLogger("prihud_logger")

    def __init__(self):
        if not DISCORD_HOOK:
            raise Exception("No discord webhook provided")

        if not self.__apr:
            self.__apr = apprise.Apprise()

        self.__apr.add(DISCORD_HOOK)

    def __del__(self):
        self.__apr.clear()
        self.__apr = None

    def info(self, body, title=None):
        self.__apr.notify(body, title, notify_type=apprise.NotifyType.INFO)
        self.__logger.info({"title": title, "body": body})

    def fail(self, body, title=None):
        self.__apr.notify(body, title, notify_type=apprise.NotifyType.FAILURE)
        self.__logger.error({"title": title, "body": body})

    def success(self, body, title=None):
        self.__apr.notify(body, title, notify_type=apprise.NotifyType.SUCCESS)
        self.__logger.info({"title": title, "body": body})

    def warn(self, body, title=None):
        self.__apr.notify(body, title, notify_type=apprise.NotifyType.WARNING)
        self.__logger.warning({"title": title, "body": body})
